# Design document
*First draft*  


*By eng10ea2, dat13sla, fpr08lni, dat13dli*



## UML Diagrams

### Client
![client](res/uml_client.png "Client")  



### Server
![server](res/uml_server.png "Server")  



## Comments

The diagrams are deliberately simple and any information we deemed unnecessary

to include or were undecided on how to implement, we omitted.

These first drafts of the design were made in pairs: one for client, one for server.

There were minimal communication between client and server pairs - mostly making sure

we had agreed on a network communication protocol - trusting in each other to get the

job done.

Synchronous viewing is determined by simply comparing timestamps of incoming images,

then displaying the two images with the least difference together.


### Network protocol
Images are simply sent over TCP as raw byte arrays with small header consisting of a 

one byte identification flag describing what type of message it is (the server may

send other data than images).  


### Some inconsistencies in the client diagram:

*   Protocol should be able to read messages from the server as well, such as:

    video mode and motion detection as well as framerate and error messages.

*   *toByte()* in Image should be *toBytes()*, and *timeStamp* should be a **long** 

    (the class is shared between both the client and server). 

*   There is support for changing the framerate in the server diagram but it didn't

    make it into the client diagram, nor did server-to-client message passing which
    
    the feature requires.


