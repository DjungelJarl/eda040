Reference Document
==================
____________________

User Guide
----------


To run the server, edit the proxy camera in the  run method in the `ImageFetcher.java` class in the server package to correspond to the camera you want to connect to e.g., 
`cam.setProxy("argus-1", 6667);`.
 After this is done, simply run the `Main.java` class, which contains a main method that starts the server.

To start the client application, run the `Main.java`, which contains the main method for the client. When the GUI appears, you can input hostname and port, and the connect to that camera. A window displaying the video feed will appear.
_____________________

Documentation of system
-----------------------
Here will follow  a brief description of the packages in the project and their respective classes.

UML of the system
------

The first diagram is the client package.

![alt text](https://bytebucket.org/DjungelJarl/eda040/raw/bf3323cbf19367c225c6374dc45a4cceef5c30ea/docs/res/client_uml.jpg?token=f61fa22318c13abd450afddf2821c1cdfaab69c7 "Client UML")

The second diagram is the server package.

![alt text](https://bytebucket.org/DjungelJarl/eda040/raw/bf3323cbf19367c225c6374dc45a4cceef5c30ea/docs/res/server_uml.jpg?token=89bad8c1035ad72083e37bde8fd3e704531aa68f "Server UML")

The third diagram is the shared package.

![alt text](https://bytebucket.org/DjungelJarl/eda040/raw/bf3323cbf19367c225c6374dc45a4cceef5c30ea/docs/res/shared_uml.jpg?token=cb26cafa2b8f09715a5687332cd1b808b7c093f2 "Shared UML")

This last diagram shows the package dependencies.

![alt text](https://bytebucket.org/DjungelJarl/eda040/raw/bf3323cbf19367c225c6374dc45a4cceef5c30ea/docs/res/package_dep_uml.jpg?token=467aa0a3427ea49cf1421a3b37ede649e1c54f43 "Package dependencies")



Server package
-------
The classes contained in the server package.

* `ImageFetcher.java` The thread responsible for establishing connection to a camera, and getting images from the camera.
* `InputHandler.java` Thread that receives commands from the client and puts them in the monitor.
* `OutputHandler.java` Thread that sends images to the client. 
* `ServerMonitor.java` Monitor for the Server. Image handling and client command handling.
* `ServerProtocol.java` Rules for handling TCP and sending images over the connection.
* `Main.java` Contains the main method which starts the server, i.e., the associated I/O and `ImageFetcher` threads.

The server retrieves images from the camera through the `ImageFetcher`, which appends a time stamp to start of the byte array that contains the picture, and stores them in the `ServerMonitor`. The monitor in turn manages an `ImageQueue`, where the images are temporarily stored, awating being sent to the client.
The `OutputHandler` retrieves an image from the `ServerMonitor` and tries to send it to a connected client. The `InputHandler` listens for commands from the client, and stores them in the monitor. The respective `ImageFetch`ers then check if any command needs to be executed, and does so if needed.

Client package
-----
The classes contained in the client package.

* `Camera.java`
* `Client.java` GUI.
* `ClientMonitor.java` Monitor for the client. Camera and image handling.
* `ClientSocket.java` Adds support for socket handling.
* `ClientTextField.java` Text handling for GUI.
* `Protocol.java` Handling connections and commands client-side.
* `ReaderThread.java` Thread for receiving images and storing them in the monitor.
* `WriterThread.java` Thread for sending commands to the servers.
* `TimerThread.java`
* `ClientImageViewer.java` Thread for retrieving/refreshing images on-screen.
* `Main.java` Contains the main method that starts the client monitor and GUI.

The `Main` class is responsible for starting the GUI and an instance of the `ClientMonitor`. Through the GUI, the user can input a hostname and port number, which when connecting will be handled through the GUI to start a connection to a server. The GUI listens for a variety of commands, such as connect, disconnect, idle mode or movie mode, which it sends through the `WriterThread`. The `ReaderThread` retrieves images from the server and through the monitor puts them in a `Camera`, which adds images to a list. The `ClientImageViewer` displays the images retrieved through the monitor, in a new window on-screen.

Shared
----------
These classes are shared between the server and the client, as they are used at both places, reducing duplicated code.

* `ImageQueue.java` Basic queue structure for handling images from the camera.
* `EmptyQueueException.java` Beautiful exception handling. No longer in use.
* `Image.java` Class describing an image with associated methods.
* `Command.java` Class describing commands being sent. Mainly used by the client.

These classes are general purpose classes. Both server and client uses the `Image` class, for instance. 