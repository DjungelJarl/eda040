EDA040 Real-Time programming project

### Workflow ###
See [Gitflow Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)

* No merge into master
* Merge into develop with pull-request
* Set up branches for features based on develop
```
#!bash
git checkout -b some-feature develop
```
* Pull before push
* After successful merge to develop, delete feature branch
