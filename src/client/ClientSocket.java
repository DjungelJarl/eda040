package client;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;

public class ClientSocket {

	private Socket s;
	private boolean f;
	private int port;
	
	public ClientSocket(Socket s, int port){
		try {
			this.s = s; this.port = port;
			f = true;
			s.setTcpNoDelay(true);
		} catch (SocketException e) {
			f = false;
			e.printStackTrace();
		}
	}
	
	public Socket getSocket(){
		return s;
	}
	
	public boolean isRunning(){
		return f;
	}
	
	public void setRunning(boolean b){
		f = b;
	}
	
	public void close() throws IOException{
		f=false;
		s.close();
	}
	
	public int getPort(){
		return port;
	}
	
	public boolean equals(Object o){
		if (o instanceof ClientSocket){
			return port == ((ClientSocket)o).port;
		}
		return false;
	}
	
	
}
