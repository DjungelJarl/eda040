package client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.WindowEvent;
import java.text.DecimalFormat;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import shared.Image;

public class ClientImageViewer extends JFrame implements Runnable {
	ImageIcon icon;
	boolean firstCall = true;
	private ClientMonitor mon;
	private int port;
	private Image prevImg;
	private Graphics graphics;
	long t0, t1;
	double fps;

	// public static void main(String[] args) {
	// ImageViewerClient viewer = new ImageViewerClient();
	// sm = new ServerMonitor();
	// ImageFetcher imgf = new ImageFetcher(sm);
	// imgf.start();
	// (new Thread(viewer)).start();
	// }

	public void run() {
		t0 = t1 = System.currentTimeMillis();
		long start = System.currentTimeMillis();
		fps = 0;
		int frames = 0;
		while (true) {

			if (!mon.isRunning(port))
				break;
			try {
				t0 = System.currentTimeMillis();
				refreshImage();
				frames++;
				
				if(frames > 2 && !mon.isMoviemode()){
//				if(System.currentTimeMillis() - start > 3000){
					fps = 1000.0*(double)frames/(System.currentTimeMillis() - start);
					frames = 0; start = System.currentTimeMillis();
				} else if (frames > 15 && mon.isMoviemode()){
					fps = 1000.0*(double)frames/(System.currentTimeMillis() - start);
					frames = 0; start = System.currentTimeMillis();
				}
				
//				t1 = System.currentTimeMillis();
//				fps = 1000.0 / (double) (t1 - t0);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		// if (mon.wasForceKilled(port))

		this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
	}

	public ClientImageViewer(ClientMonitor mon, int port) {
		super();
		this.mon = mon;
		this.port = port;
		getContentPane().setLayout(new BorderLayout());
		icon = new ImageIcon();
		JLabel label = new JLabel(icon);
		add(label, BorderLayout.CENTER);
		this.pack();
		this.setSize(640, 480);
		this.setVisible(true);
		prevImg = null;
		graphics = this.getGraphics();
	}

	public void refreshImage() throws InterruptedException {
		java.awt.Image image = null;

		Image img = mon.getImage(port);// blocking
		if (mon.wasForceKilled(port)) {
			System.out.println("FORCE KILLED");
			return;
		}
		if (img == null) return;

		image = getToolkit().createImage(img.getJPEG());
		getToolkit().prepareImage(image, -1, -1, null);

		icon.setImage(image);

		icon.paintIcon(this, graphics, 0, 0);
		graphics.setColor(Color.MAGENTA);
		graphics.drawString("FPS: " + new DecimalFormat("##.##").format(fps) + "\n", 10, 55);
		
		if(mon.isMoviemode()){
			graphics.drawString("mode: moviemode", 10, 67);
		} else {
			graphics.drawString("mode: idle mode", 10, 67);
		}
		
		long delay = System.currentTimeMillis() - img.getTimestamp();
		graphics.drawString("delay: " + delay + "ms", 10, 79);
		
		mon.setDelay(port, delay);
		
		if(mon.isSynchronous()){
			graphics.drawString("synchronous", 10, 96);
		} else {
			graphics.drawString("asynchronous", 10, 96);
		}
		
		prevImg = img;

	}
}