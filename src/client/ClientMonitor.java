package client;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import shared.Image;

public class ClientMonitor {
	private HashMap<Integer, Camera> cameraMap;
	private Set<Integer> forceKilledCameras;
	private boolean moviemode;
	private boolean synchronous;

	public ClientMonitor() {
		cameraMap = new HashMap<>();
		forceKilledCameras = new HashSet<>();
		moviemode = false;
		synchronous = false;
	}

	public synchronized boolean addCamera(int id) {
		if (cameraMap.containsKey(id))
			return false;
		cameraMap.put(id, new Camera());
		return true;
	}

	public synchronized void putImage(int id, Image image) {
		Camera temp = cameraMap.get(id);
		temp.putImage(image);
		cameraMap.put(id, temp);
		notifyAll();
	}

	public synchronized Image getImage(int id) throws InterruptedException {
		while (!forceKilledCameras.contains(id) && cameraMap.get(id) != null && cameraMap.get(id).getSize() == 0)
			wait();
		if (forceKilledCameras.contains(id) || cameraMap.get(id) == null) {
			return null;
		}
		return cameraMap.get(id).removeFirst();
	}

	public synchronized void removeCamera(int id) {
		Camera temp = cameraMap.get(id);
		if (temp != null) {
			temp.setRunning(false);
			cameraMap.put(id, temp);
		}
		notifyAll();
	}

	public synchronized boolean isRunning(int id) {
		if (cameraMap.get(id) == null)
			return false;
		return cameraMap.get(id).isRunning();
	}

	public synchronized void setWriting(int id, boolean b) {
		cameraMap.get(id).setWriting(b);
		if (!b)
			notifyAll();
	}

	public synchronized void awaitWriteComplete(int id) throws InterruptedException {
		while (cameraMap.get(id).isWriting())
			wait();
	}

	public synchronized void forceKilled(int id) {
		forceKilledCameras.add(id);
		removeCamera(id);
		kill(id);
	}

	public synchronized boolean wasForceKilled(int id) {
		return forceKilledCameras.contains(new Integer(id));
	}

	public synchronized void clearForceKilled(int id) {
		forceKilledCameras.remove(id);
	}

	public synchronized void kill(int id) {
		cameraMap.remove(id);
		notifyAll();
	}

	public synchronized void setMoviemode(boolean b) {
		if (b != moviemode) {
			for (int id : cameraMap.keySet()) {
				Camera temp = cameraMap.get(id);
				temp.setModeChanged(true);
				cameraMap.put(id, temp);
			}
		}
		moviemode = b;
		notifyAll();

	}

	public synchronized void modeChanged(int id) throws InterruptedException {
		while (cameraMap.get(id) == null || !cameraMap.get(id).modeChanged())
			wait();
		Camera temp = cameraMap.get(id);
		temp.setModeChanged(false);
		cameraMap.put(id, temp);
		notifyAll();
	}

	public synchronized boolean isMoviemode() {
		return moviemode;
	}

	public synchronized void setDelay(int id, long l) {
		if (cameraMap.get(id) != null) {
			Camera temp = cameraMap.get(id);
			temp.setDelay(l);
			cameraMap.put(id, temp);

			int[] arr = new int[cameraMap.size()];
			int i = 0;
			for (int key : cameraMap.keySet()) {
				if (cameraMap.get(key).getDelay() != 0) {

					arr[i] = (int) cameraMap.get(key).getDelay();

				} else {
					synchronous = false;
					return;
				}
			}
			int min = minDifference(arr);
			if (min < 200) {
				synchronous = true;
			} else {
				synchronous = false;
			}
		}
	}

	public synchronized long getDelay(int id) {
		return cameraMap.get(id).getDelay();
	}

	public synchronized boolean isSynchronous() {
		return synchronous;
	}

	public static int minDifference(int[] A) {
		Arrays.sort(A);
		if (A.length > 1) {
			int d = Math.abs(A[0] - A[1]);
			for (int i = 0; i <= A.length; i++) {
				if (i + 1 < A.length) {
					int t = Math.abs(A[i] - A[i + 1]);
					if (t < d)
						d = t;
				}
			}
			return d;
		}
		return -1;
	}

}
