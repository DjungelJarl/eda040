package client;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import shared.Command;

public class WriterThread extends Thread {
	private ClientMonitor mon;
	private ClientSocket cs;
	private OutputStream os;
	private DataOutputStream dos;
	private int port;

	public WriterThread(ClientMonitor mon, ClientSocket cs) {
		this.cs = cs;
		this.mon = mon;
		this.port = cs.getPort();
	}

	public void run() {
		try {
			os = cs.getSocket().getOutputStream();
			dos = new DataOutputStream(os);
			while (cs.isRunning()) {
				mon.modeChanged(port);
				mon.setWriting(port, true);
				if (!mon.isRunning(port))
					sendCmd(new Command(Command.DISCONNECT).toByte());
				else {
					if (mon.isMoviemode()) {
						System.out.println("Sending MOVIEMODE");
						sendCmd(new Command(Command.MOVIEMODE).toByte());
					} else {
						sendCmd(new Command(Command.RESUME).toByte());
						System.out.println("Sending RESUME");
					}
				}
				mon.setWriting(port, false);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void sendCmd(byte cmd) throws IOException {
		sendBytes(new byte[] { cmd }, 0, 1);
	}

	public void sendBytes(byte[] myByteArray, int start, int len) throws IOException {
		if (len < 0)
			throw new IllegalArgumentException("Negative length not allowed");
		if (start < 0 || start >= myByteArray.length)
			throw new IndexOutOfBoundsException("Out of bounds: " + start);
		// Other checks if needed.

		// May be better to save the streams in the support class;
		// just like the socket variable.
		DataOutputStream dos = new DataOutputStream(os);

		dos.writeInt(len);
		if (len > 0) {
			dos.write(myByteArray, start, len);
		}
	}

}
