package client;

import java.util.ArrayList;

import shared.Image;

public class Camera {

	private ArrayList<Image> images;
	private boolean running;
	private boolean writingTo;
	private boolean modeChanged;
	private long delay;
	
	public Camera(){
		images = new ArrayList<>();
		running = true;
		writingTo = false;
		modeChanged = false;
		delay = 0;
	}
	
	public boolean isRunning(){
		return running;
	}
	
	public boolean isWriting(){
		return writingTo;
	}
	
	public void setRunning(boolean b){
		running = b;
	}
	
	public void setWriting(boolean b){
		writingTo = b;
	}
	
	public Image removeFirst(){
		return images.remove(0);
	}
	
	public void putImage(Image img){
		images.add(img);
	}
	
	public int getSize(){
		return images.size();
	}
	
	public boolean modeChanged(){
		return modeChanged;
	}
	
	public void setModeChanged(boolean b){
		modeChanged = b;
	}
	
	public void setDelay(long l){
		delay = l;
	}
	
	public long getDelay(){
		return delay;
	}
}
