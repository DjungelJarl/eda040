package client;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.AbstractButton;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Client extends JFrame implements ActionListener {
	private ImageIcon icon;
	boolean firstCall = true;
	private ClientMonitor mon;
	private JButton defaultbutton;
	private JButton moviebutton;
	private ArrayList<ClientTextField> ips;
	private ArrayList<ClientTextField> ports;
	private ArrayList<JButton> buttons;
	private int connectionID = 0;
	private GridLayout fieldsPanel;
	private final JPanel compsToExperiment;
	private Protocol p;

	public Client(ClientMonitor mon) {
		super();
		this.mon = mon;
		p = new Protocol(mon);
		fieldsPanel = new GridLayout(0, 3);
		ips = new ArrayList<ClientTextField>();
		ports = new ArrayList<ClientTextField>();
		buttons = new ArrayList<JButton>();
		compsToExperiment = new JPanel();
		compsToExperiment.setLayout(fieldsPanel);
		JPanel controls = new JPanel();
		controls.setLayout(new GridLayout(2, 3));
		ips.add(new ClientTextField(connectionID, "hacke-6"));
		ports.add(new ClientTextField(connectionID, "59556"));
		compsToExperiment.add(new JLabel("host"));
		compsToExperiment.add(new JLabel("port"));
		compsToExperiment.add(new JLabel("action"));
		compsToExperiment.add(ips.get(0));
		compsToExperiment.add(ports.get(0));
		JButton b1 = new JButton("connect");
		b1.setActionCommand("connect" + connectionID);
		b1.addActionListener(this);
		b1.setMnemonic(KeyEvent.VK_ENTER);
		buttons.add(b1);
		compsToExperiment.add(b1);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		controls.add(new Label(" "));
		controls.add(new Label(" "));
//		controls.add(new Label(" "));

		defaultbutton = new JButton("Idle mode");
		defaultbutton.setVerticalTextPosition(AbstractButton.CENTER);
		defaultbutton.setHorizontalTextPosition(AbstractButton.LEADING);
		defaultbutton.setMnemonic(KeyEvent.VK_D);
		defaultbutton.setActionCommand("idle");
		defaultbutton.addActionListener(this);
		
		moviebutton = new JButton("Movie mode");
		moviebutton.setVerticalTextPosition(AbstractButton.CENTER);
		moviebutton.setHorizontalTextPosition(AbstractButton.LEADING);
		moviebutton.setMnemonic(KeyEvent.VK_M);
		moviebutton.setActionCommand("movie");
		moviebutton.addActionListener(this);

		controls.add(moviebutton);
		controls.add(defaultbutton);
		this.mon = mon;
		getContentPane().setLayout(new BorderLayout());
		icon = new ImageIcon();
		JLabel label = new JLabel(icon);
		add(label, BorderLayout.CENTER);
		this.pack();
		this.setSize(640, 480);
		this.setVisible(true);

		add(compsToExperiment, BorderLayout.NORTH);
		add(controls, BorderLayout.SOUTH);
	}

	private void addComponents() {
		ips.add(new ClientTextField(connectionID));
		ports.add(new ClientTextField(connectionID));
		JButton b1 = new JButton("connect");
		b1.setActionCommand("connect" + connectionID);
		b1.addActionListener(this);
		b1.setMnemonic(KeyEvent.VK_ENTER);
		
		buttons.add(b1);
		compsToExperiment.add(ips.get(ips.size() - 1));
		compsToExperiment.add(ports.get(ports.size() - 1));
		compsToExperiment.add(b1);
		compsToExperiment.revalidate();
		compsToExperiment.repaint();
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("idle")) {
			mon.setMoviemode(false);
		}else if(e.getActionCommand().equals("movie")){
			mon.setMoviemode(true);
		}
		else {
			for (int i = 0; i < buttons.size(); i++) {
				if (e.getActionCommand().equals(buttons.get(i).getActionCommand())) {
					System.out.println("Connecting to " + ips.get(i).getText() + ":" + ports.get(i).getText());
					if (buttons.get(i).getText().equals("connect")) {
						try {
							if (p.connect(ips.get(i).getText(), ports.get(i).getPort())) {
								ips.get(i).setEnabled(false);
								ports.get(i).setEnabled(false);
								buttons.get(i).setText("disconnect");
								buttons.get(i).setMnemonic(KeyEvent.VK_ESCAPE);
								mon.addCamera(Integer.parseInt(ports.get(i).getText()));
								ClientImageViewer viewer = new ClientImageViewer(mon,
										Integer.parseInt(ports.get(i).getText()));
								viewer.addWindowListener(new WindowAdapter() {
									public void windowClosing(WindowEvent evt) {
										onExit();
										viewer.dispose();
									}
								});
								new Thread(viewer).start();
								connectionID++;
								addComponents();
							} else {
								JOptionPane.showMessageDialog(null, "Error connecting");
							}
						} catch (NumberFormatException e1) {
							JOptionPane.showMessageDialog(null, "Error connecting port nr must be int");
							// e1.printStackTrace();
						}
					} else {
						try {
							int port = Integer.parseInt(ports.get(i).getText());
							if (p.disconnect(port)) {
								mon.removeCamera(port);
								if (buttons.size() > 1) {
									removeRow(i);
								} else {
									ips.get(i).setEnabled(true);
									ports.get(i).setEnabled(true);
									buttons.get(i).setText("connect");
									buttons.get(i).setMnemonic(KeyEvent.VK_ENTER);

								}

							} else {
								JOptionPane.showMessageDialog(null, "Error disconnecting");
							}
						} catch (NumberFormatException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (HeadlessException e1) {
							// TODO Auto-generated catch block
							// e1.printStackTrace();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							// e1.printStackTrace();
						}

					}

				}

			}
		}
	}
	
	private void onExit() {
	
		int i=0;
		for (ClientTextField port : ports){
			System.out.println(port.getPort());
			if(mon.wasForceKilled(port.getPort())){
				JOptionPane.showMessageDialog(null, "Disconnected: connection lost at port " + port.getPort(), "Error",
						JOptionPane.ERROR_MESSAGE);
				removeRow(i);
				mon.clearForceKilled(port.getPort());
			}
			i++;
		}
		
	}
	
	private void removeRow(int id){
		compsToExperiment.remove(ips.get(id));
		compsToExperiment.remove(ports.get(id));
		compsToExperiment.remove(buttons.get(id));
		ips.remove(id);
		ports.remove(id);
		buttons.remove(id);
		compsToExperiment.revalidate();
		compsToExperiment.repaint();
	}

}
