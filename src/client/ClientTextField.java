package client;

import javax.swing.JTextField;

public class ClientTextField extends JTextField{
	private int id;
	public ClientTextField(int id){
		this.id=id;
	}
	
	public ClientTextField(int id, String port){
		super.setText(port);
		this.id=id;
	}
	
	public int getID(){
		return id;
	}
	
	public int getPort(){
		//System.out.println(super.getText());
		try{
			return Integer.parseInt(super.getText());
		}catch(Exception e){
			return -1;
		}
		
	}
}
