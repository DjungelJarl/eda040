package client;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;

import shared.Command;
import shared.Image;

public class ReaderThread extends Thread {
	private ClientMonitor mon;
	private ClientSocket cs;
	private InputStream is;
	private int port;

	public ReaderThread(ClientMonitor mon, ClientSocket cs) throws IOException {
		this.cs = cs;
		is = cs.getSocket().getInputStream();
		this.mon = mon;
		port = cs.getPort();
	}

	public void run() {
		try {
			while (cs.isRunning()) {
				byte[] data = readBytes();
				if (data.length > 16) {
					byte[] temp = new byte[data.length - 1];
					for(int i = 0; i < data.length - 1; i++){
						temp[i] = data[i + 1];
					}
					int cmd = data[0];
					if((int)cmd == Command.MOVIEMODE){
						mon.setMoviemode(true);
					} 
 					mon.putImage(port, new Image(temp));					
				}
			}
			mon.awaitWriteComplete(port);
			cs.close();
			mon.kill(port);
		} catch (Exception e) {
			if (e instanceof EOFException){
				 mon.forceKilled(port);
				 System.out.println("DISCONNECTED");
				 try {
					cs.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			} else {
				e.printStackTrace();
			}
			//throw new IOException();

		}
		
	}
	
	public byte[] readBytes() throws IOException {
	    DataInputStream dis = new DataInputStream(is);

	    int len = dis.readInt();
	    byte[] data = new byte[len];
	    if (len > 0) {
	        dis.readFully(data);
	    }
	    return data;
	}
}
