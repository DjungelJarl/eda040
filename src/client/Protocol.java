package client;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;

import shared.Command;

public class Protocol {
	private ArrayList<ClientSocket> sockets;
	private int portNumber;
	private InputStream in;
	private OutputStream out;
	private ClientMonitor mon;
	
	public Protocol(ClientMonitor mon) {
		this.mon = mon;
		sockets = new ArrayList<ClientSocket>();
	}
	
	public boolean connect(String host, int portNr) {
		Socket socket;
		try {
			socket = new Socket(host, portNr);
			in = socket.getInputStream();
			out = socket.getOutputStream();
			socket.setTcpNoDelay(true);
			ClientSocket cs = new ClientSocket(socket,portNr);
			if(sockets.contains(cs)){
				int i = sockets.indexOf(cs);
				if(!sockets.get(i).isRunning()){
					sockets.remove(i);
					sockets.add(cs);
					new ReaderThread(mon,cs).start();
					new WriterThread(mon,cs).start();
					return true;
				}
				socket.close();
				return false;
			}
			sockets.add(cs);
			new ReaderThread(mon,cs).start();
			new WriterThread(mon,cs).start();
			return true;
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
		//	e.printStackTrace();
			return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			return false;
		}
		
	
	}
	

	public boolean disconnect(int portNr) throws IOException{
			Iterator<ClientSocket> itr = sockets.iterator();
			while(itr.hasNext()){
				ClientSocket cs = itr.next();
				if(cs.getPort()==portNr){
					cs.setRunning(false);
					sockets.remove(cs);
					return true;
				}
			}
			return false;
	}
	
	public void closeAll() throws IOException{
		Iterator<ClientSocket> itr = sockets.iterator();
		while(itr.hasNext()){
			itr.next().close();
		}
	}

	public void writeCmd(Command cmd) throws IOException{
		byte temp[] = new byte[1];
		temp[0]=cmd.toByte();
		writePacket(temp);
	}
	
	private void writePacket(byte[] data) throws IOException {
		int size = data.length;
		byte hi = (byte)(size / 255);
		byte lo = (byte)(size % 255);
		
		out.write(hi);
		out.write(lo);
		out.write(data, 0, size);
		
		out.flush();
	}
}
