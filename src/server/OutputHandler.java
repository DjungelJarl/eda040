package server;

import java.io.IOException;
import java.net.SocketException;

import shared.Image;

public class OutputHandler extends Handler {
	public OutputHandler(ServerProtocol sp, ServerMonitor sm) {
		super(sp, sm);
	}

	protected void _run() {
		try {
			Image img = sm.getImage();

			int cmd = sm.getSendCommand();
			if (img != null) {
				byte[] imgByteArray = img.toBytes();
				byte[] byteArray = new byte[img.toBytes().length + 1];
				byteArray[0] = (byte) cmd;

				for (int i = 1; i < byteArray.length; i++) {
					byteArray[i] = imgByteArray[i - 1];
				}

				sp.sendBytes(byteArray, 0, byteArray.length);
			}
		} catch (SocketException e) {
			kill();
			return;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
