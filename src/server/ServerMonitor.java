package server;

import shared.*;

public class ServerMonitor {
	public final static int IDLE_DELAY = 1000 * 2;
	
	private boolean threadsAlive;
	
	private int command;
	private int sendCommand;
	private boolean movieMode;
	private Image img;

	public ServerMonitor() {
		movieMode = false;
		sendCommand = 0;
		threadsAlive = true;
	}

	public synchronized void putImage(Image img) {
		this.img = img;
		if (movieMode) notifyAll();
	}

	public synchronized Image getImage() throws InterruptedException {
		wait(ServerMonitor.IDLE_DELAY);
		return img;
	}

	public synchronized void setMovieMode(boolean on) {
		movieMode = on;
	}
	
	public synchronized boolean movieMode() {
		return movieMode;
	}

	public synchronized void setClientCommand(int c) {
		command = c;
	}

	public synchronized int getClientCommand() {
		return command;
	}
	
	public synchronized void setSendCommand(int c) {
		sendCommand = c;
	}

	public synchronized int getSendCommand() {
		return sendCommand;
	}
	
	public synchronized boolean areThreadsAlive() {
		return threadsAlive;
	}
	public synchronized void killThreads() {
		threadsAlive = false;
	}
}
