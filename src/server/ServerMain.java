package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerMain {
	public static void main(String[] args) {
		int serverPort =  Integer.parseInt(args[0]);//59556;
		String cameraHost = args[1];//"argus-1.student.lth.se";
		int cameraPort = Integer.parseInt(args[2]);//6666;
		try {
			while (true) {
				ServerSocket ss = new ServerSocket(serverPort);
				ss.setReuseAddress(true);
				System.out.println("Listening on port " + serverPort);	
				Socket socket = ss.accept();
				
				ServerMonitor sm = new ServerMonitor();
				ServerProtocol sp = new ServerProtocol(socket, sm);
				InputHandler in = new InputHandler(sp, sm);
				OutputHandler out = new OutputHandler(sp, sm);
				ImageFetcher imgf = new ImageFetcher(sm, cameraHost, cameraPort);
				
				imgf.start();
				in.start();
				out.start();
				
				while (imgf.isAlive() || in.isAlive() || out.isAlive()) {}
				
				ss.close();
				Thread.sleep(3000);
				System.out.println("Restarting...");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
