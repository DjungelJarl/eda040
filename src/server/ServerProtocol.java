package server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class ServerProtocol {
	private Socket socket;
	private InputStream input;
	private OutputStream output;
	
	public ServerProtocol(Socket s, ServerMonitor sm) {
		socket = s;
		try {
			input = socket.getInputStream();
			output = socket.getOutputStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void close() throws IOException {
		input.close();
		output.close();
		socket.close();
	}
	
	public byte[] readBytes() throws IOException {
	    DataInputStream dis = new DataInputStream(input);

	    int len = dis.readInt();
	    byte[] data = new byte[len];
	    if (len > 0) {
	        dis.readFully(data);
	    }
	    return data;
	}

	public void sendBytes(byte[] myByteArray, int start, int len) throws IOException {
	    DataOutputStream dos = new DataOutputStream(output);

	    dos.writeInt(len);
	    if (len > 0) dos.write(myByteArray, start, len);
	}
}
