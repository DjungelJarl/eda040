package server;

import java.io.IOException;

abstract class Handler extends Thread {
	protected ServerMonitor sm;
	protected ServerProtocol sp;
	
	protected Handler(ServerProtocol sp, ServerMonitor sm) {
		this.sm = sm;
		this.sp = sp;
	}
	
	// signals all threads to terminate
	// in case of a connection error
	protected void kill() {
		try {
			sp.close();
			sm.killThreads();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Connection closed");
	}
	
	public void run() {
		while (sm.areThreadsAlive()) _run();
		kill();
	}
	
	protected abstract void _run();
}
