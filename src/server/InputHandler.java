package server;

import java.io.EOFException;
import java.io.IOException;
import java.net.SocketException;


public class InputHandler extends Handler {
	public InputHandler(ServerProtocol sp, ServerMonitor sm) {
		super(sp, sm);
	}

	protected void _run() {
		try {
			int command = (int) sp.readBytes()[0];
			sm.setClientCommand(command);
		} catch (EOFException e) { 
			kill();
			return;
		} catch (SocketException e) {
			kill();
			return;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
