package server;

import shared.*;

import se.lth.cs.eda040.proxycamera.AxisM3006V;

public class ImageFetcher extends Thread {
	private ServerMonitor sm;
	private AxisM3006V cam;
	private String host;
	private int port;

	public ImageFetcher(ServerMonitor sm, String cameraHost, int cameraPort) {
		this.sm = sm;
		cam = new AxisM3006V();
		host = cameraHost;
		port = cameraPort;
	}

	public void run() {
		cam.init();
		cam.setProxy(host, port);
		cam.connect();
		while (sm.areThreadsAlive()) {
			processCommand();
			if (cam.motionDetected()) {
				sm.setMovieMode(true);
				sm.setSendCommand(Command.MOVIEMODE);
			} else {
				sm.setSendCommand(0);
			}
			sm.putImage(fetchImage());
		}
	}
	
	private void processCommand() {
		int cmd = sm.getClientCommand();
		if (cmd == Command.DISCONNECT) {
			cam.close();
			cam.destroy();
			return;
		} else if (cmd == Command.RESUME) {
			sm.setMovieMode(false);
		} else if (cmd == Command.MOVIEMODE) {
			sm.setMovieMode(true);
		}
	}
	
	private Image fetchImage() {
		byte[] jpeg = new byte[AxisM3006V.IMAGE_BUFFER_SIZE];
		byte[] timeStamp = new byte[Image.TIMESTAMP_SIZE];
		cam.getJPEG(jpeg, 0);
		cam.getTime(timeStamp, 0);
		return new Image(timeStamp, jpeg);
	}
}
