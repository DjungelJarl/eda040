package shared;

public class Image {
	public static final int TIMESTAMP_SIZE = 8; // assuming 64-bit timestamp

	private byte[] timestamp;
	private byte[] jpeg;
	private int size;

	public Image(byte[] timestamp, byte[] jpeg) {
		this.timestamp = timestamp;
		this.jpeg = jpeg;
		size = jpeg.length + timestamp.length;
	}

	public Image(byte[] raw) {
		size = raw.length;
		timestamp = new byte[TIMESTAMP_SIZE];
		jpeg = new byte[size - TIMESTAMP_SIZE];
		for (int i = 0; i < TIMESTAMP_SIZE; i++)
			timestamp[i] = raw[i];
		for (int i = 0; i < jpeg.length; i++)
			jpeg[i] = raw[i+TIMESTAMP_SIZE];
	}

	public byte[] toBytes() {
		byte[] bytes = new byte[size];
		for (int i = 0; i < TIMESTAMP_SIZE; i++)
			bytes[i] = timestamp[i];
		for (int i = TIMESTAMP_SIZE; i < size; i++)
			bytes[i] = jpeg[i-TIMESTAMP_SIZE];
		return bytes;
	}
	
	public long getTimestamp() {
		long time = 0;
		for (int i = 0; i < timestamp.length; i++) {
			time = (time << 8) + (timestamp[i] & 0xff);
		}
		return time;
	}

	public byte[] getJPEG() {
		return jpeg;
	}

	public int size() {
		return size;
	}
}
