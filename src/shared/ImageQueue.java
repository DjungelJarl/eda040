package shared;

/*
 *	A dynamic queue for images
 */
public class ImageQueue {
	private ImageNode first, last;
	private int size;

	public ImageQueue() {
		first = last;
		size = 0;
	}

	public void put(Image img) {
		if (isEmpty()) {
			first = new ImageNode(img);
		} else if (size == 1) { 
			last = new ImageNode(img);
			first.next = last;
		} else {
			last.next = new ImageNode(img);
			last = last.next;
		}
		size++;
	}

	public Image get() {
		if (first != null) {
			Image temp = first.img;
			first = first.next;
			size--;
			return temp;
		}
		return null;
	}

	public boolean isEmpty() {
		return first == null;
	}
	
	public int size() {
		return size;
	}
	
	private class ImageNode {
		ImageNode next;
		Image img;
		
		public ImageNode(Image img) {
			this.img = img;
			next = null;
		}
	}
}
