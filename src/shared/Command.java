package shared;

public class Command {
	
	public static final int ERROR = 2;
	public static final int DISCONNECT = 3;
	public static final int RESUME = 4;
	public static final int MOVIEMODE = 5;
	
	private int command;
	
	public Command(int command){
		this.command = command;
	}
	
	public byte toByte(){
		switch(command){
			case DISCONNECT:
				return (byte)DISCONNECT;
			case RESUME:
				return (byte)RESUME;
			case MOVIEMODE:
				return (byte)MOVIEMODE;
			default:
				return (byte)ERROR;
		}
	}
}
